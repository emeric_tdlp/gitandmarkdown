# :rocket: Mars Rover Project :ringed_planet:
> :memo: *Essay by Léonille Coculet*

<!-- ![CuriosityRover](https://user-images.githubusercontent.com/84009746/156787927-794d6a92-21e1-4ff5-a719-b50a3ffacc2e.png) -->
<p align="center"> 
  <img src="https://user-images.githubusercontent.com/84009746/156787927-794d6a92-21e1-4ff5-a719-b50a3ffacc2e.png"  width="300" height="400"/> 
</p>
<!-- I really wanted to center this image and make it smaller so I used an html tag. -->

## 1. Project description

### :dart: The project's goal  
The *Mars Rover project* consists of creating an autonomous motorized system used from a remote location just like *Curiosity[^1]*, the rover on Mars for 10 years.
The functionalities to be designed were:
- **detect** and **avoid** obstacles
- **map** his local work area on a remote database
- design a **charging station**
- set up the station to **recharge the batteries** and **power the rover**

### :star2: Why this project?

I came across this project by accident while looking for an application of the *DE10-lite electronic board* that we got this year. I chose this project because it is complete, besides I found interesting the fact that it was created by *electronics students* at Imperial College London.


## 2. Repository description

#### :information_source: On the repository[^2] we can find this information:
- The owner is **JosiahMendes**.
- **5 contributors** participated.
- **222 commits** were made.
- As the project is **completed**, there is **2 branches** 
   - the *main* branch
   - a "*BiberBranch*" where the work is obviously disorganized
- There are **9 directories** : about one directory per system functionality (*energy, vision...*).
- **2 actions** were used
   - "checkout"
   - "upload-artifact"


#### :keyboard: Languages that were used:
| Language              | Percent (%) |
| :---                  |    :----:   |
| C                     | 44.8        |
| Verilog               | 18.9        |
| VHDL                  | 9.5         |
| Makefile              | 8.7         |
| SystemVarilog         | 8.3         |
| C++                   | 3.4         |
| Other                 | 6.4         |



## 3. Pull requests and Issues
As this is a project that had to be returned by a specific date, there are no more new issues and no more new pull requests.
However, we can see that they used 9 different labels to find their way around the pull requests.

[^1]: [Curiosity](https://fr.wikipedia.org/wiki/Curiosity_(rover)) is a rover deployed at Aeolis Palus on Mars in 2012.
[^2]: The project's repository is available [here](https://github.com/JosiahMendes/Autonomous-Rover).

