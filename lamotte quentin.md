# The Sapphire Telemetry system

## Copenhagen Suborbitals
<hr>____________________________________________________________________________ 

![Copenhagen_subrobitals_logo](https://upload.wikimedia.org/wikipedia/commons/f/f3/Copenhagen_Suborbitals_logo.png)

 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;The Sapphire Telemetry System is the system that was created by Copenhagen Suborbitals to see if they could create a guidance system for their future projects.You can find their
 repository [here](https://github.com/csete/stlm).
 
 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Copenhagen Suborbitals is a non profit and open source organisation based in Copenhagen,Denmark and founded in 2008 who create home-built rocket for fun.
55 amateur work during their spare time to create those rocket. They currently have succesfully launched six liquid propellant rocket and reach a max altitude of
more than 10Km, their rocket are the only liquid propellant rocket made by amateur. The crowd-funded associacion short budget of around 10 000€/mounth, wich is about 10% of NASA's budget for coffee per month, makes them an interesting place for space agency as they to have a limited budget and can see what idea are created when
the budget is very short. For instance they helped the ESA with their hook on philae because they work with a technology and Copenhagen Suborbitals noticed that
this technology do not worked propelly in low pressure environment.



#### This particular projet is about creating a telemetry system to feed a guidance system for some of their rocket.
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;In order to do that they must gather many data during the
flight such as the altitude, the attitude of the rocket, its position, the pressure around the rocket... It must also be able to communicate those data to the ground, as the launcher is in testing its chance of failure is really high and after this this might not be possible to recover the gathered data. Finally it must also be able to recieve
some data.
 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;I have choosen this project because I like the aerospatial industry and Copenhagen Suborbitals is an unique project that is conducted by amateur that are not specialist of the 
 aerospatial domain but rather some pasionate people that gather around what they love.


#### The repository is organised as follow:
The repository is conposed of 4 major documents that describe a part of the project:
 - doc
 - receiver
 - system
 - transmitter
 
 
 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;The core sytem and a doc show the interaction between the rocket and the ground staff. As the team that work in each department is pretty small in Copenhagen Suborbitals there was only one contributor to post on this github, but at least 3 contributed to the code. 202 commits were done and 29,400K additions and 3,920K deletion were done. 
 
 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;The project used git as a sharing platform of the working files for the teams
of Copenhagen Suborbitals to be able to have an easy way to find all the information they need about the system. As it is an open source association this also the way for them to share their work. This repository do not need external docuentation everything can be found here, the datasheets of the circuit board used are listed in the repository. Three version are present here.
