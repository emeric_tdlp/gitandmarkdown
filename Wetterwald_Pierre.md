# Pandas
![Pandas logo](pandas_logo.png)
## What's panda ?
---
Pandas is an open-source Python package. It's designed to ease data manipulation and analysis. It's build among others over Numpy, another Python package.

It comes with some built-in data types such as DataFrames and Series. It allows a good handling of missing datas, easy operations on those structures (merging, slicing, reshaping, grouping).

Except *Numpy*, Pandas uses few dependencies (*python-dateutils* and *pytz*).

Pandas is built over several different languages. Here are the mains used :

- Python
- C
- Cython

You can find Pandas Github repository clicking on [this link](https://github.com/pandas-dev/pandas) to get further informations.
## Pandas repository architecture
---
Pandas repository is composed of 20 separated branches. 13 of them are stable release versions, 2 are reverts, and the other five are miscellaneous content. 

| Branches | Content|
|-------------|-----------|
|13|Stable releases|
|2|Reverts|
|5|Miscellaneous|

They made the choice to use an interesting feature, **tags**. Tags are textual labels to set a specific name to a project under version control.

They used it in a different way. Each release including new features are in separated branches, and tags are used to index errors corrections versions.

As described, pandas project is using [**semantic versioning**](www.semver.org).
## Pull Requests ans issues management
---
As it is an open-source project, there are a lot of pull requests (107 at that moment, and 23,970 closed). They are all named using the same semantic :

>[Name of the modified part][Modification]

The collaborators who are submitting a pull request are invited to take care of not generating merging errors. This would result in an instant refusal of the request.