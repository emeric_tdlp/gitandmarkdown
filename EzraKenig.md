# **Software developpement : report on TheAlgorithms' C repository**


![TheAlgorithms' logo](https://avatars.githubusercontent.com/u/20487725?s=200&v=4)


## I/ Introduction
The Algorithms is a Collective of programmers counting 90 members. On their GitHub page (which can be consulted [right here](https://github.com/TheAlgorithms)), they provide several repositories ; each indexes algorithms written in a specific programming language such as:

- Python
- C++
- Java

Here, the example of their soberly but righteously named repository C will be taken.

## II/ Description
As it can be expected, every file of the repository is written in C or a derived version of C:

| Language | Usage |
| :------: | :---: |
| C | 97.0 % |
| Cmake | 2.6 % |
| Others | 0.4 % |

Other information can be seen on the repository's web page:

| Entity | Quantity |
| :---: | :---: |
| Contributors | 206 |
| Stars | 12.6k |
| Forks | 3.3k |
| Branches | 5 |

The repository has no other purpose than providing a reliable and consisitent index of algorithms for programmers of any level.

## III/ Pull requests
The repository has 13 pull requests which majorly consist in additions of algorithms,debugging and improvements of already existing algorithms.
