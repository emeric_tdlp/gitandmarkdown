# Arduino-IRremote
# Table of the content
1. Introduction
2. Gerenal information
3. Collaborators or contributors
4. Commits
5. Issues
6. Acces

## Introduction
The well known public repository **Arduino-IRremote** is a library for arduino that allows to send and receive infrared signals with protocols like Denon / Sharp, JVC, LG, NEC / Onkyo / Apple, Panasonic / Kaseikyo, RC5, RC6, Samsung, Sony etc. I personally used it when I was working on a project to control a light bulb via a remote control by using the famous Arduino *Uno R3* board and an infrared sensor.That is the main reason i choose it for my report.Most of the functions in the repository are coded in **C++** language but the contributors also use **C** to code some.
## General information
Here is more information about **Arduino-IRremote**:
+ There are **2** main branches **master** and **gh-pages**
+ The current number of versions is **24**. The last one version is **v3.6.1**
+ It can have up to 220 watchers at the same time
+ the network is huge. More than **1000** network’s repositories.
+ 19 workflows runs in the last 2 months. all are libraries
## Collaborators
Like most repositories on github, **ARDUINO-IRremote** has **53** *Collaborators* worldwide but only **21** are actually active. The following table shows a classification by country:
|USA|Germany|Canada|Sweden|UK|Norway|Argentina|Italy|Poland|Unknown country|
|---|-------|------|------|--|------|---------|-----|------|---------------|
|8|3|1|1|1|1|1|1|1|3|
## Commits
This graph shows the history of commits from **March 2021** to the last week of **February 2022**.
![alt text](Commit.jpg)
The total number of commits on the repository is **680**.The passionate Canadian developer **Rafi Khan** has to his credit **91** commits. He is the first to have worked the most on the directory and was the main contributor untill the version 2.4.0. The last who made the *commit* of the last version is ArminJo the present maintainer of the repository.
Considering the data of the last 4 weeks reported on the graph, we have an average of 4 commits per week.
## Issues
In total, **597** issues have already been resolved. Only **1** is still open, this is the bug in the IrReceiver.stop() function. 
The ArminJo maintainer is the one who is responsible for helping users solve their problems.

## Acces
The following link lead to the repository
[Go To the Arduino-IRremote repository](https://github.com/Arduino-IRremote/Arduino-IRremote)
