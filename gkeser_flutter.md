# Flutter Repository

![Flutter](https://raw.githubusercontent.com/flutter/website/archived-master/src/_assets/image/flutter-lockup-bg.jpg)
 

<p>&nbsp;</p>


## Table of contents

1. Presentation of the project
    1. What it is about 
    2. Why I chose this repository
2. Description of the repository
3. Pull Request & Issue Management

## Presentation of the project

#### What is it about :

**Flutter** is a free and open-source mobile UI framework created by **Google** and released in May 2017. In a few words, it allows you to create a native mobile application with only one codebase. This means that you can use one programming language and one codebase to create two different apps (for **iOS** and **Android**). 

There are many advantages to using Flutter :

- Easy to learn
- Increased productivity
- Cost-effective
- Great documentation & community

#### Why I chose this repository : 

I think the project itself is very useful for developers who need to adapt their code on multiple platforms without spending time on programming different things for each platform, and it might be interesting to talk about this open-source software development kit.

## Description of the repository

Currently, the open-source UI framework **Flutter** has 9 repositories on **GitHub** but I will present the main repository called *flutter*.

*You can access Flutter's repository [here](https://github.com/flutter/flutter)* 

The repository is aimed at engineers building or making contributions. Everyone is welcome to contribute code via pull requests, to file issues, to help people asking for help on mailing lists or chat channels. Or on Stack Overflow, to help triage, reproduce, or fix bugs that people have filed, or to help out in any other way. 

Contributors can find on the repository many files of documentation, for instance, such as *Install Flutter*, *Widgets Catalogs* or *Contributing to Flutter*.

Here are some statistics on the repository :

| Projects | Branches | Commits | Contributors |
| -------- | -------- | -------- | -------- |
| 171    | 289     | 27789  | 983    |

Here are some percentages concerning the use of programming languages by contributors :

|      | Dart | C | C++ | Java | Shell | CMake | Others |
| -------- | -------- | -------- | -------- | -------- | -------- | -------- | -------- |
| Percentages    | 99.2    | 0.2   | 0.1    | 0.2    | 0.1    | 0.1    | 0.1    |

Dart is mainly used by the contributors, this is object-oriented language that can be used on multiple platforms.


## Pull Request & Issue Management

- We currently count 10,301 open issues and 55,350 closed issues.
- And there are 174 open PR and 33,223 closed PR.

GitHub is a suitable way for Flutter and his contributors to report some bugs or problems. The issue tracker contains the list of bugs reported by the community. Contributors use labels to specify the type of issues, for example, the label *severe:* can indicates labels regarding a level of severity. Or the label *Px* (with x=0..6) refers to priorities (0 for the most important, to 6 for the least important).
Concerning PR, contributors have to follow some instructions before offering their solutions such as read some guidelines, making sure that their PR passes all the pre-commit tests, or get reviewed by a regular Flutter contributor.

- **To conclude**, the goal is to enable developers to deliver high-performance apps that feel natural on different platforms, embracing differences where they exist while sharing as much code as possible.



