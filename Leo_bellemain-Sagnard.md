# Git and Markdowns Evaluation Project
Bellemain-Sagnard Léo SNUM1
___
## Table of contents    
1. Presentation of the project
2. Description of the repository
3. Structure of the project
___
## Presentation of the project
[![Vim Logo](https://github.com/vim/vim/raw/master/runtime/vimlogo.gif)](https://www.vim.org)

[Vim](https://www.vim.org/) is a text editor. It is directly inspired by Vi, which was widely used on Unix operating 
systems. Vim offers many features accessible via a keyboard shortcut. This makes it possible not to use the mouse and to
keep the hands on the keyboard and thus to save time.

This [repository](https://github.com/vim/vim) has to date :

| Category                           | Number |
|------------------------------------|:------:|
| Watchs (participating and mentions |  685   |
| Forks                              |   4k   |
| Stars                              | 26.4k  |
| Issues open                        |  1096  |
| Issues closed                      |  4913  |
| Pull requests open                 |   85   |
| Pull requests closed               |  3779  |
| Releases                           | 13873  |
| Workflow runs                      | 11182  |

___
## Description of the repository
This repository was opened on 13 June 2004. There are currently 98 contributors with over 15k commits. You can find all
the information about Vim on their website : https://www.vim.org/.

___
## Structure of the project
Pulls and requests are classified by 39 labels. This allows anyone to make changes to the code to fix problems or add
new features. Each request is examined by both project contributors and volunteers. Each request is presented as a blog
on Github, which allows an easier exchange and collaboration. 

Let's take this application as an example: https://github.com/vim/vim/pull/9795
It currently has 28 out of 30 checks validated before it can be integrated into the project. 6 commits have been made
and 20 changes have been made to the proposed files