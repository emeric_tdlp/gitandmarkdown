# **[yuzu](https://yuzu-emu.org/), Nintendo Switch emulator**

[![the yuzu logo](https://raw.githubusercontent.com/yuzu-emu/yuzu-assets/master/icons/icon.png)](https://yuzu-emu.org/)



##### **_The world's most popular, open source, Nintendo Switch emulator_**
---

yuzu is a project developed with the aim of being able to run the games marketed on Nintendo Switch.
Indeed, yuzu is a free and open-source Nintendo Switch emulator, developed in C++, and designed and updated to run on both Windows and Linux.

This project is based on the principles of:
* Reverse Engineering 
* Switch development 
    * [Switch Hardware and Software](https://switchbrew.org/w/index.php?title=Main_Page)
    * [Switch’s CPU architecture](https://developer.arm.com/documentation/ddi0487/ha/)

The [project](https://github.com/yuzu-emu/yuzu) was mostly developed on GitHub where many contributors helped to highlight it and move it forward. And it can boast since 2019 of being able to emulate games launched on the market, and of being able to run them with a resolution close to the original as long as the user has the appropriate equipment. A [list](https://yuzu-emu.org/game/) of compatible games and their level of performance is also available. 

##### **_Description of the repository_**
---  

| forks | stars | issues | pull requests |
|:---------------:|:---------------:|:---------------:|:---------------:| 
| 1.6k  |  19.3k  |  890  |  48  |


##### **_Why this project_**
---

I chose this project because it is an ambitious project that has succeeded and has many supporters/contributors. In addition, compared to other repositories, this project is based not on innovation but on reverse engineering, which brings yet another approach to the pooling of research and development provided by each contributor. Also, being a consumer of the types of games emulated myself, this one caught my attention directly.

##### **_Source_**
---

[GiHub : yuzu's repository](https://github.com/yuzu-emu/yuzu)
  
    
    
    
    
    
      
                                                                        Corentin BENELLI
          
          




