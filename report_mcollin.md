# GIT Repository Report
[_VLC Media Player_](https://github.com/videolan/vlc)

<img src="https://www.logo.wine/a/logo/VLC_media_player/VLC_media_player-Logo.wine.svg" width=20% height=20%>
   
## The Open Source Project
  
* _What is it?_
 
 VLC Media Player is a libre and open source media player and multimedia engine. You can play everything anywhere. This media player can play many multimedia files, discs, streams, devices and also, you can convert, encode, stream and manipulate streams into various formats. It used all around the world.
 The engine of VLC can be embedded into 3rd party applications (_libVLC_). libVLC is linked to other languages such as C++, Python and C#.
 Also, VLC is part of the [VideoLAN project](https://www.videolan.org/) (created at the university [Ecole Centrale Paris](https://www.centralesupelec.fr/)) and is developed and suported by a community of volunteers. There are 561 contributors in this project. Moreover, the language C is the language most used by developpers (66.1%), then it's C++ (18.5%) and Objective-C (6.3%). Other languages are used like QML, Makefile, Lua but they are in the minority.

 * _Why do I choose VLC Media Player?_

I want to know more about this application and how it is made. I usually use this to read some movies and I'm curious about what else we can do.

## Description of the Repository

 First of all, VLC project is composed of two branches: the main branch called "master" and a second one called "3.0.x". It can be summarize like that:


|   Branches    |  Contents    |
| ----------- | ----------- |
| Master Branch  | folders, files |
| 3.0.x Branch   | folders, files |


 * _Main branch_

In this branch, you will find many folders like "modules", "lib"... It's where you find all the most important documents of the project. Also, you will find the "README.md" file which succinctly describes the project. Moreover, there is the installation file of the application. This branch represent the official version of VLC Media Player. Finally, it's the most active branch of the project because it is modified every day.

 * _3.0.x branch_

This branch contains the last version of VLC Media Player with also files which contains the previous version of the app. Its composition is the same as the main branch but it contains more older files and the branch is updated less often.

 * _Pull requests, tags, updates and releases_
 
1. Pull request: 123 pull request closed and the last one was 28 days ago. There are approximately monthly pull requests. 
2. Tags: there are 90 at the time of writing this report and just like the pull requests, there are some every month. These tags concern the 3.0.x version of VLC Media Player.
3. Updates and Releases: we don't have informations about that but we can say that project is worked on every days by developpers.

## Conclusion

In a nutshell, VLC Media Player is a project that is worked on every month but is less active than other larger projects such as VSCode by Microsoft or VIM but it is still well organised.

