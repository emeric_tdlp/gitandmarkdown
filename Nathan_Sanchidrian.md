# **The MicroPython project**
![Micropython](https://upload.wikimedia.org/wikipedia/commons/thumb/4/4e/Micropython-logo.svg/300px-Micropython-logo.png)

You can find the link to go on the repository [here:](https://github.com/micropython/micropython)

## **Table of contents**

1. What is the project
2. Description of the repository
3. Pull request and issue management
4. Why did I choose this Repository


## *1. What is the project*

This page is for an implementation of Python 3.x on microccontrollers and small embedded systems.




## *2. Description of the repository*

|Number of collaborators|Commits|Forks|Branches|Watch| 
|--|--|--|--|--|
|417|13161|5200|5|745|

There are 5 differents branches, the master and 4 others ones :

- reentrant-gc
- cross
- parse-bytecode
- docs-dev

|**language**|C|Python|makefile|C++|Cmake|shell|Other|
|--|--|--|--|--|--|--|--|
|Number in %|89,4|8,7|1|0,4|0,2|0,2|0,1|

We can notice that they used C to implement Python in microccontrollers. I found this really paradoxical. 🥴

## *3. Pull request and issue management*
This repository have two differents parts which are really interresting, *issue* and *pull request*.    
Issue is the place where users are telling their problems and pull request is the place where collaborators are discuting and trying changements before integrate them to the final project.    
There are a lot of issues and pull request which are open but there are more issue and pull request which are closed than open! That is a great thing, isn't it? In fact, there are 1150 open's issue against 2922 which are closed. And 277 open's pull request against 4026 which are closed.

## *4. Why did I choose this Repository*
To finish this essai, I'll tell you why I choose this repository. It's because I'm really interristing about microccontrollers and small embedded. Bytheway, i would like to work in recherche and devellopment on small embedded. This is an expanding sector and I would like to be part of this expansion if it's possible.
